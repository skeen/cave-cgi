#!/bin/bash

echo "Content-type: text/plain"
echo ""

echo -e "Started image tester... \c"
echo "Ok"


echo -e "Starting image releaser... \c"
./docker-image-test/bootstrap.sh &
echo "Ok"

exit 0
