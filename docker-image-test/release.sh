#!/bin/bash

NEW_TAG=$(docker images | head -3 | grep -v "latest" | tail -1 | grep -o "\S*\s*" | head -2 | tail -1)

docker tag -f chrisgulddahl/skycave-rc:$NEW_TAG skeen/staging-socket:$NEW_TAG 
docker tag -f skeen/staging-socket:$NEW_TAG skeen/staging-socket:latest
docker push skeen/staging-socket
