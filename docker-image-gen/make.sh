#!/bin/sh

NEW_TAG_FILE=new.tag
OLD_TAG_FILE=old.tag
CONTAINER_NAME=image-builder
DOCKERHUB_REPO=skeen/staging-socket

## Update repository
if [ ! -d "cave-dev" ]; then
    git clone git@bitbucket.org:skeen/cave-dev.git
    cd cave-dev
else
    cd cave-dev
    git pull
fi

## Get the latest tag
git describe --abbrev=0 --tags > ../$NEW_TAG_FILE
cd ..

## Read the tagfiles
NEW_TAG=$(cat $NEW_TAG_FILE)
OLD_TAG=$(cat $OLD_TAG_FILE)

echo "new_tag: $NEW_TAG"
echo "old_tag: $OLD_TAG"

if [ "$NEW_TAG" != "$OLD_TAG" ]; then
    echo "-------------------------"
    echo "New tag found; releasing!"
    echo "-------------------------"

    ## Make sure image-builder is up
    # Stop it if it's running
    IMAGE_BUILDER=$(docker ps | grep "image-builder")
    if [ -z "$IMAGE_BUILDER" ]; then
        echo "Image-builder isn't running, downloading and starting it"
    else
        echo "Image-builder is running; resetting it"
        docker stop image-builder
    fi
    # Remove it if it's stopped
    IMAGE_BUILDER_STOPPED=$(docker ps -a | grep "image-builder")
    if [ -n "$IMAGE_BUILDER_STOPPED" ]; then
        docker rm image-builder 
    fi
    # Download and start a new one
    docker run -d -it --name image-builder skeen/staging-socket:v2

    # Create a tar file of the source tree
    echo "Creating tar file...\c"
    tar -cof cave-dev.tar cave-dev/

    # Remove the current source tree from the daemon
    echo "ok\nRemoving docker source tree...\c"
    docker exec $CONTAINER_NAME rm -rf /root/cave/

    # Push the updated tar to the daemon
    echo "ok\nSending tar file to docker...\c"
    docker exec -i $CONTAINER_NAME /bin/bash -c 'cat > /root/cave-dev.tar' < cave-dev.tar

    # Unpack the new tar
    echo "ok\nUnpacking tar on docker...\c"
    docker exec $CONTAINER_NAME /bin/bash -c "cd /root/; tar -xf cave-dev.tar"

    # Move the directory to have the correct naming
    echo "ok\nMoving source tree to active tree...\c"
    docker exec $CONTAINER_NAME /bin/bash -c "cd /root/; mv cave-dev cave"

    # Get the commit message of the tag
    echo "ok\nAcquireing release details...\c"
    COMMIT_MSG=$(git tag -l -n $NEW_TAG | sed 's/^$NEW_TAG\s*//g')
    AUTHER="CSS13"

    # Create docker image and tag it
    echo "ok\nCreating release candidate image...\c"
    docker commit -m "$COMMIT_MSG" -a "$AUTHER" $CONTAINER_NAME $DOCKERHUB_REPO:$NEW_TAG
    docker tag -f $DOCKERHUB_REPO:$NEW_TAG $DOCKERHUB_REPO:latest

    # Uploading release candidate image
    echo "ok\nCreating release candidate image...\c"
    docker push $DOCKERHUB_REPO
    echo "ok"

    cp $NEW_TAG_FILE $OLD_TAG_FILE
else
    echo "Tag is already pushed, nothing to do!"
fi
