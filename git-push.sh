#!/bin/bash

echo "Content-type: text/plain"
echo ""

echo -e "Starting image builder... \c"

./docker-image-gen/bootstrap.sh &

echo "Ok"
exit 0
